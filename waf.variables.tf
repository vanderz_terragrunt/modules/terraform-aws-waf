variable "waf_name" {}

variable "scope" {}

variable "rules" {}

variable "cloudwatch_metrics_enabled" {}

variable "metric_name" {}

variable "sampled_requests_enabled" {}

variable "resource_arn_association" {}
